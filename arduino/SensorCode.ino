// Libraries needded for ethernet connections
#include <SPI.h>         
#include <Ethernet.h>
#include <EthernetUdp.h> 


String unique_identifier= "00-HH-11-00-HH-11"; //Arduino-Sensor Unique Identifier
long pulse, inches, cm; // Distance variables for sensor measurments
const int pwPin = 7; // Digital pin where sensor is connected
int onLed=13; // Led

byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; //Arduino Mac Address variable
String local_ip; // Arduino Ip Address
unsigned int listen_Port = 5005; // Port used by the arduino to listen for incoming UDP broadcasts
unsigned int send_Port = 5006;  // Port used by the arduino to send informatio to the Raspberry Pi A.K.A the "HOME SERVER"
IPAddress server_ip; // Raspberry Pi A.K.A the "HOME SERVER"'s IP address
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; // Char Buffer variable used to recieve information from the "HOME SERVER"
char  ReplyBuffer[100];//Char Buffer variable used to send information to the "HOME SERVER"

boolean is_connected; // Boolean that states its value depending if Arduino is communicated with the "HOME SERVER" is_connected=True => is connected
EthernetUDP Udp; // Variable use to initialize UDP connections

void setup() { // Method used to set up pins and UDP connections

  Ethernet.begin(mac); //Method called in order to begin ethernet in the Arduino 
  Udp.begin(listen_Port); //Method callec in order to initialize UDP connections to listen 
  Serial.begin(9600);
  pinMode(13,OUTPUT);
  connect(); //Method called to start listening for incoming broadcasts
  
}

void loop() { //Method that loops sending distance data to the "HOME SERVER" once the first connection is Established
  pinMode(pwPin, INPUT); // Sensor initialization in Pin=pwPin
  pulse = pulseIn(pwPin, HIGH);// Method used to obtain sensords Data (distance)
  inches = pulse/147;
  cm = inches * 2.54;
  String send_info="ui:"+unique_identifier+";data:"+cm; //String construction of Data to be sent in the form of "ui:unique_identifier;data:distancce"
  Serial.print(send_info);
  send_info.toCharArray(ReplyBuffer,100);//String buffered to a char array
  Udp.beginPacket(server_ip, send_Port);// Udp package initialization in order to send a buffer char
  Udp.write(ReplyBuffer);// Method used to write to the package to be sent
  Udp.endPacket();//Method used to flush the udp package and finish it
  
  delay(5000); //delay of 5 seconds 
}

void connect(){ //Method executed until first connection with HOME SERVER is established
  local_ip= String(Ethernet.localIP()); 
  while(!server_ip){ //loop that remains while Arduino is listening for the first broadcast
    int packetSize = Udp.parsePacket();// Listening for incoming UDP packages
    if(packetSize)
    {
      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      Serial.print("From ");
      server_ip = Udp.remoteIP(); //saving the "HOME SERVER'S" IP Address in memory
      for (int i =0; i < 4; i++)
      {
        Serial.print(server_ip[i], DEC);
        if (i < 3)
        {
          Serial.print('.');
        }
      }
      Serial.print(", port ");
      Serial.println(Udp.remotePort());

      Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
      Serial.println("Contents:");
      Serial.println(packetBuffer);
      
  
    }
    delay(10);
  }
  Udp.begin(send_Port);
  is_connected=true;// Acknowledgin succesfull connection to "HOME SERVER"
  digitalWrite(onLed, HIGH); // Turn led on to show a successfull connection
}
