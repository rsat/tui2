# -*- coding: utf-8 -*-

"Service who broadcast the HomeServer location on the network"

from socket import *
cs = socket(AF_INET, SOCK_DGRAM)
cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
cs.sendto('HomeServer broadcast', ('255.255.255.255', 5005))