# -*- coding: utf-8 -*-

"""
Service than runs in the Home Server and is in charge of receiving data from sensor
and sending the data to the Main Server.
"""

from socket import *
import sys
import select
import MySQLdb
from httplib2 import Http
from urllib import urlencode

server_controller = "http://192.168.1.106:8000/livs/home_server/receive_data"
http = Http()

# Sends data to the main server
def send_data(mac_address, data, h):
	data = dict(mac_address=mac_address, data=data)
	resp, content = h.request(server_controller, "POST", urlencode(data))

# Create connection to the DB
db = MySQLdb.connect("localhost", "arduino", "123456", "home_server")
curs = db.cursor()

# Load data into memory
sensors = {}
sensors_ids = {}

curs.execute("SELECT mac_address, last_value, id FROM sensors WHERE last_value <> '' AND last_value IS NOT NULL")
for row in curs.fetchall():
	sensors[row[0]] = row[1]
	sensors_ids[row[0]] = row[2]

# Creates socket for receiving data from sensors
host="0.0.0.0"
port = 5006
s = socket(AF_INET,SOCK_DGRAM)
s.bind((host,port))

addr = (host,port)
buf=1024

# Starts listening for data 
while True:
        data,addr = s.recvfrom(buf)
        data = data.strip()
        data = data.split(";")
        if len(data) == 1: # New connection
			print "Sensor connected", data
        elif len(data) == 2: # New data received
			print "New Data", data
			ui = data[0].split(":")
			new_data = data[1].split(":")
			if len(ui) == 2 and len(new_data) == 2: 
				mac_address = ui[1]
				n_data = new_data[1]
				try:
					if mac_address in sensors.keys(): # If the sensor is allowed to send data
						print n_data, 
						if n_data != sensors[mac_address]: # We check that the new data is different than the previos received
							# If data has changed, we send it to the main server, and update the database
							curs.execute("UPDATE sensors SET last_value='{0}', updated_at=NOW() WHERE id='{1}'".format(n_data, sensors_ids[mac_address]))
							send_data(mac_address, n_data, http)
							sensors[mac_address] = n_data
							db.commit()
					else:
						# We check if there is an update on the database
						curs.execute("SELECT id FROM sensors WHERE mac_address = '{0}'".format(mac_address))
						result = curs.fetchone()
						if result:
							# If there has been an update on the database, and the sensor was added, we loaded on memory and save the data
							id = result[0]
							curs.execute("UPDATE sensors SET last_value='{0}', updated_at=NOW() WHERE id='{1}'".format(n_data, id))
							db.commit()
							send_data(mac_address, n_data, http)
							sensors[mac_address] = n_data
							sensors_ids[mac_address] = id
				except Exception, e:
					print "Error", e
					db.rollback()
