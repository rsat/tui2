# -*- coding: utf-8 -*-

"""
Web Server thats run in the HomeServer, and provides de interface so the MainServer can interact
with the HomeServer
"""

import json
import time
import BaseHTTPServer
import MySQLdb

db = MySQLdb.connect("localhost", "arduino", "123456", "home_server")
curs = db.cursor()

HOST_NAME = 'livs.com'
PORT_NUMBER = 80 

# Method which creates a dictionary with the vars on the URL
def get_vars(path, startswith):
    vars = {}
    vars_list = path.replace(startswith, "").split("&")
    for v in vars_list:
        key, value = v.split("=")
        vars[key] = value
    return vars

# The WebServer implementation
class HomeServer(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        if s.path.startswith("/add_sensor?"): # Add Sensor

            vars = get_vars(s.path, "/add_sensor?")

            if "mac_address" in vars.keys() and "sensor_type" in vars.keys():
                s.send_response(200)
                s.send_header("Content-type", "application/json")
                s.end_headers()
                try:
                    curs.execute("INSERT INTO sensors (mac_address, sensor_type) values('{0}','{1}')".format(vars["mac_address"], vars["sensor_type"]))
                    db.commit()

                    result = {  
                                "message" : "successful",
                                "status" : 200
                            }

                    s.wfile.write(json.dumps(result))

                except:
                    db.rollback()
                    result = {  
                                "message" : "mac_address already exist",
                                "status" : 500,
                            }
                    s.wfile.write(json.dumps(result))
            else:
                s.send_error(500,"Bad Request")

        elif s.path.startswith("/remove_sensor?"): # Remove Sensor

            vars = get_vars(s.path, "/remove_sensor?")

            if "mac_address" in vars.keys():
                s.send_response(200)
                s.send_header("Content-type", "application/json")
                s.end_headers()
                try:
                    curs.execute("DELETE FROM sensors WHERE mac_address = '{0}'".format(vars["mac_address"]))
                    db.commit()

                    result = {  
                                "message" : "successful",
                                "status" : 200
                            }

                    s.wfile.write(json.dumps(result))

                except:
                    db.rollback()

        elif s.path.startswith("/set_main_server_ip?"): # Set or Change Main Server Ip

            vars = get_vars(s.path, "/set_main_server_ip?")

            if "server_ip" in vars.keys():
                s.send_response(200)
                s.send_header("Content-type", "application/json")
                s.end_headers()
                try:
                    print "#escribir en un archivo"

                    result = {  
                                "message" : "successful",
                                "status" : 200
                            }

                    s.wfile.write(json.dumps(result))

                except:
                    print "Error"
                    s.send_error(500,"Bad Request")

        elif s.path.startswith("/last_reading?"): # Get Last Reading from Sensor

            vars = get_vars(s.path, "/last_reading?")

            if "mac_address" in vars.keys():
                s.send_response(200)
                s.send_header("Content-type", "application/json")
                s.end_headers()
                try:
                    curs.execute("SELECT last_value FROM sensors WHERE mac_address='{0}'".format(vars["mac_address"]))
                    last_value = curs.fetchone()

                    result = {  
                                "last_value" : last_value[0],
                                "status" : 200
                            }

                    s.wfile.write(json.dumps(result))


                except:
                    print "Error"
                    s.send_error(500,"Bad Request")


        elif s.path.startswith("/is_sending_data?"): # Get If Some Sensor is Sending Data 

            vars = get_vars(s.path, "/is_sending_data?")

            if "mac_address" in vars.keys():
                s.send_response(200)
                s.send_header("Content-type", "application/json")
                s.end_headers()
                try:
                    curs.execute("SELECT (NOW() - updated_at) < 1800 FROM sensors where mac_address = '{0}'".format(vars["mac_address"]))
                    updated_at = curs.fetchone()


                    result = {  
                                "is_sending_data" : updated_at == "1",
                                "status" : 200
                            }

                    s.wfile.write(json.dumps(result))


                except:
                    print "Error"
                    s.send_error(500,"Bad Request")

        else:
            s.send_error(404,'File Not Found: %s' % s.path)

# Main 
if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class(('', PORT_NUMBER), HomeServer)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
