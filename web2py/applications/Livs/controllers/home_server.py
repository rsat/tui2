from datetime import *

#Controller that adds a new sensor to the current user
#It communicates with the Raspberry Pi using urllib2, if done successfully adds a new sensor to the local db
def add_sensor():
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))
	connection_problem = True	
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	if the_user.home_server_ip is None or the_user.home_server_port is None:
		redirect(URL('home_server', 'configure_server'))
	rows = db().select(db.sensor_types.ALL)
	sensor_types_select = SELECT(_name='sensor_type')
	is_positive_select = SELECT(_name='is_positive')
	is_positive_select.append(OPTION("Yes", _value="True"))
	is_positive_select.append(OPTION("No", _value="False"))
	
	for row in rows:
		sensor_types_select.append(OPTION(row.sensor_type, _value=row.sensor_type))
		
	form=FORM('MAC address:',
						INPUT(_name='mac_address'),
						BR(),
						'Sensor type:',
						sensor_types_select,
						BR(),
						'Is positive:',
						is_positive_select,
						BR(),
						'Description:',
						INPUT(_name='description'),
						BR(),
						INPUT(_type='submit'))
	if form.accepts(request,session):
		mac_address = request.vars.mac_address.replace(":", "-")
		sensor_type = request.vars.sensor_type
		is_positive = request.vars.is_positive
		if is_positive == "True":
			is_positive = True
		else:
			is_positive = False
		home_server_ip = the_user.home_server_ip
		home_server_port = the_user.home_server_port
		url = "http://{}:{}".format(home_server_ip,home_server_port)+"/add_sensor?mac_address="+mac_address+"&sensor_type="+sensor_type
		import urllib2
		try:
			message = urllib2.urlopen(url).read()
			#print message			
			sensor_type_id = db(db.sensor_types.sensor_type==sensor_type).select().first().id
			existing_sensor = db(db.sensors.mac_address==mac_address).select().first()
			if existing_sensor is None or existing_sensor.user_id == auth.user.id:
				db.sensors.update_or_insert(db.sensors.mac_address==mac_address, mac_address=mac_address,sensor_type=sensor_type_id,user_id=auth.user.id,is_positive = is_positive)
				connection_problem = False
			else:
				response.flash = 'mac address already exists'
				connection_problem = True
		except Exception as e:
			print str(e)
			response.flash = 'Server respond with an error'
		if not connection_problem:
			redirect(URL('home_server','set_normal_value',vars=dict(mac_address = mac_address)))
	elif form.errors:
		response.flash = 'form has errors'
	else:
		response.flash = 'please fill the form'
	return dict(form=form)

#Controller that list all the sensor that the user have.
#The user pick one and then communicates with the Raspberry Pi using urllib2, if done successfully remove the sensor to the local db
def remove_sensor():
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))
	connection_problem = True
	rows = db(db.sensors.user_id==auth.user.id).select()
	sensors_select = SELECT(_name='mac_address')	
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	if the_user.home_server_ip is None or the_user.home_server_port is None:
		redirect(URL('home_server', 'configure_server'))
	
	for row in rows:
		sensors_select.append(OPTION(row.mac_address, _value=row.mac_address))
	if len(rows) < 1:
		form=FORM('No sensors available')
	else:
		form=FORM('MAC address:',
							sensors_select,
							BR(),
							INPUT(_type='submit'))
	if form.accepts(request,session):
		mac_address = request.vars.mac_address
		home_server_ip = the_user.home_server_ip
		home_server_port = the_user.home_server_port
		url = "http://{}:{}".format(home_server_ip,home_server_port)+"/remove_sensor?mac_address="+mac_address
		import urllib2
		try:
			message = urllib2.urlopen(url).read()
			db(db.sensors.mac_address==mac_address).delete()
			connection_problem = False
		except Exception as e:
			print str(e)
			response.flash = 'Server respond with an error'
		if not connection_problem:
			redirect(URL('default', 'index'))
	elif form.errors:
		response.flash = 'form has errors'
	else:
		response.flash = 'please fill the form'
	return dict(form=form)

#Controller that list all the sensor from the current user
#It communicates with Raspberry Pi for each sensor to see if the sensor measure a change the last 30 minutes*
def list_sensors():
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))	
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	if the_user.home_server_ip is None or the_user.home_server_port is None:
		redirect(URL('home_server', 'configure_server'))
	rows = db(db.sensors.user_id==auth.user.id).select()
	if len(rows) < 1:
		sensors_table = "You have no sensors"
	else:
		home_server_ip = the_user.home_server_ip
		home_server_port = the_user.home_server_port
		sensors_table = TABLE(TR(TH('mac address'),TH('type'),TH('status')))
		sensors_types_rows = db().select(db.sensor_types.ALL)
		sensors_types = {}
		import urllib2
		for row in sensors_types_rows:
			sensors_types[str(row.id)] = row.sensor_type
		for row in rows:
			try:
				url = "http://{}:{}".format(home_server_ip,home_server_port)+"/is_sending_data?mac_address="+row.mac_address
				message = urllib2.urlopen(url).read()
				import json
				message = json.loads(message)
				status = "disconnected"
				if message["is_sending_data"]:
					status = "connected"
				sensors_table.append(TR(TD(row.mac_address),TD(sensors_types[str(row.sensor_type)]),TD(status),TD(A("Set normal value", _href=URL('home_server','set_normal_value',vars=dict(mac_address = row.mac_address))))))
			except Exception as e:
				print str(e)
				sensors_table.append(TR(TD("error"),TD("error"),TD("error")))
	return dict(sensors_table=sensors_table)

#Controller to set the home server ip, mac address and port
def configure_server():
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	form=FORM('Server IP:',
						INPUT(_name='home_server_ip',_value=the_user.home_server_ip , requires=IS_MATCH('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', strict=True)),
						BR(),
						'Server port:',
						INPUT(_name='home_server_port',_value=the_user.home_server_port, requires=IS_MATCH('^0*(?:6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{1,3}|[0-9])$', strict=True)),
						BR(),
						'MAC address:',
						INPUT(_name='home_server_mac',_value=the_user.home_server_mac, requires=IS_MATCH('^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$', strict=True)),
						BR(),
						INPUT(_type='submit'))
						
	if form.accepts(request,session):		
		db.auth_user.home_server_ip.writable = True
		db.auth_user.home_server_port.writable = True
		db.auth_user.home_server_mac.writable = True	
		db.auth_user.home_server_ip.readable = True
		db.auth_user.home_server_port.readable = True
		db.auth_user.home_server_mac.readable = True
		try:
			db(db.auth_user.id == auth.user.id).update(home_server_ip=request.vars.home_server_ip,home_server_port=request.vars.home_server_port,home_server_mac=request.vars.home_server_mac)
		except Exception as e:
			print str(e)		
		db.auth_user.home_server_ip.writable = False
		db.auth_user.home_server_port.writable = False
		db.auth_user.home_server_mac.writable = False
		db.auth_user.home_server_ip.readable = False
		db.auth_user.home_server_port.readable = False
		db.auth_user.home_server_mac.readable = False
		redirect(URL('default', 'index'))
	elif form.errors:
		response.flash = 'form has errors'
	else:
		response.flash = 'please fill the form'
	return dict(form = form)

def avatar():
	update_avatar()
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	return dict(first_name=auth.user.first_name, last_name=auth.user.last_name,level=the_user.level,strength=the_user.strength,dexterity=the_user.dexterity,vitality=the_user.vitality)	

#Controller to set the normal value of a sensor (calibrate)
def set_normal_value():	
	if not auth.is_logged_in():
		redirect(URL('default', 'index'))
	mac_address = request.vars.mac_address
	the_sensor = db(db.sensors.mac_address==mac_address).select().first()
	if the_sensor.user_id == auth.user.id:
		the_user = db(db.auth_user.id==auth.user.id).select().first()
		home_server_ip = the_user.home_server_ip
		home_server_port = the_user.home_server_port
		url = "http://{}:{}".format(home_server_ip,home_server_port)+"/last_reading?mac_address="+mac_address
		import urllib2
		message = urllib2.urlopen(url).read()
		import json
		message = json.loads(message)
		last_value = message["last_value"]
		form=FORM('Actual value:',
					last_value,
					BR(),
					BR(),
					INPUT(_type='submit'))
	else:
		redirect(URL('default', 'index'))
	if form.accepts(request,session):
		last_value
		try:
			db(db.sensors.id == the_sensor.id).update(normal_data=last_value)
		except Exception as e:
			print str(e)
	elif form.errors:
		response.flash = 'form has errors'
	else:
		response.flash = 'please fill the form'
	return dict(form=form, mac_address=mac_address)

#Service that the home server (Raspberry Pi) uses to send the data
def receive_data():
	mac_address = request.vars.mac_address
	data = request.vars.data
	the_sensor = db(db.sensors.mac_address==mac_address).select().first()
	try:
		db.sensor_data.insert(sensor_id=the_sensor.id, data=data)
		return True
	except Exception as e:
		print str(e)
		return False


def update_avatar(): #Method used to update a users avatar
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	the_sensors=db(db.sensors.user_id==auth.user.id).select()
	final_add=[]
	for sensor in the_sensors:
		sensor_data=db((db.sensor_data.sensor_id==sensor.id) & (db.sensor_data.created_on>the_user.last_update)).select()
		sensor_type=db(db.sensor_types.id==sensor.sensor_type).select().first()
		if sensor_type.sensor_type=="task_recognition":
			if(len(sensor_data)>0):
				update_final=task_recognition_update(sensor,sensor_data)
				final_add.append(update_final)

	for data in final_add:
		if data!=None:
			db.sensor_data.insert(sensor_id=data[0],data=data[1],created_on=request.now+ timedelta(0,3))
	db.commit()				

def task_recognition_update(sensor,sensor_data): #method used for task recognition sensor ie: watching tv
	normal_data=sensor.normal_data
	update_final=None
	time=0
	if sensor.is_positive:
				None
	else:
		for d in range(0,len(sensor_data)):
			time1=sensor_data[d].created_on
			time2=request.now
			if(d+1<=len(sensor_data)-1):
				time2=sensor_data[d+1].created_on
			else:
				if(sensor_data[d].data!=normal_data):
					update_final=[sensor.id,sensor_data[d].data]		
			if(normal_data!=sensor_data[d].data):
				time+=(time2-time1).seconds / 60
			update_vitality(time,False)	
	print time
	db(db.auth_user.id==auth.user.id).update(last_update=request.now)
	db.commit()			
	return update_final

def update_vitality(score,is_positive): #Method used to update the vitality of the user
	the_user = db(db.auth_user.id==auth.user.id).select().first()
	vitality=the_user.vitality
	if(is_positive):
		db(db.auth_user.id==auth.user.id).update(vitality=vitality+score)
	else:
		db(db.auth_user.id==auth.user.id).update(vitality=vitality-score)	




