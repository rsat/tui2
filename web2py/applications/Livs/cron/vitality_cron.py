vitality_upgrade=10

def vitality_cron(): #Cron tab used to update vitality every hour
	users=db(db.auth_user).select()
	for user in users:
		the_user = db(db.auth_user.id==user.id).select().first()
		vitality=the_user.vitality
		db(db.auth_user.id==user.id).update(vitality=vitality+vitality_upgrade)


vitality_cron()