# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
		db = DAL('postgres://livs:123456@localhost:5432/livs', migrate=True)
    #db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
    #db = DAL(['sqlite://storage.sqlite','postgres://livs:123456@localhost:5432/livs'],pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

auth.settings.extra_fields[auth.settings.table_user_name] = [ 
																															Field('home_server_ip', type='string', readable=False, writable=False), 
																															Field('home_server_port', type='string', readable=False, writable=False), 
																															Field('home_server_mac', type='string', readable=False, writable=False),
																															Field('vitality', type='integer', readable=False, writable=False, default=100), 
																															Field('strength', type='integer', readable=False, writable=False, default=0), 
																															Field('dexterity', type='integer', readable=False, writable=False, default=0), 
																															Field('level', type='integer', readable=False, writable=False, default=0),
                                                                                                                            Field('last_update',type='datetime',readable=False,writable=False,default=request.now) 
																														]

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')
T.force("en-us")
#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################
																														
#db.define_table('sensors', migrate=True,fake_migrate=True)
#db.define_table('sensor_types', migrate=True,fake_migrate=True)

db.define_table('sensor_types',
        Field('sensor_type', type='string', notnull=True, unique=True)
        )
				
db.define_table('sensors',
        Field('mac_address', type='string', notnull=True, unique=True),
        Field('sensor_type', type=db.sensor_types),
        Field('user_id', type=db.auth_user),
        Field('is_positive',type='boolean',notnull=True),
        Field('normal_data',type='string',notnull=True, default='')
        )

db.define_table('sensor_data',
        Field('sensor_id', type=db.sensors),
        Field('data', type='string', notnull=True),
        Field('created_on', type='datetime', default=request.now)
        )

				
## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
