# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(B('Livs'),XML('&trade;&nbsp;'),
                  _class="brand",_href= URL('default', 'index'))
response.title = request.application.replace('_',' ').title()
response.subtitle = ''

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Your Name <you@example.com>'
response.meta.description = 'a cool new app'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Home'), False, URL('default', 'index'), [])
]

if auth.is_logged_in():
    response.menu += [
    (T('My Sensors'), False, URL('home_server', 'list_sensors'), []),
    (T('Add Sensor'), False, URL('home_server', 'add_sensor'), []),
    (T('Remove Sensor'), False, URL('home_server', 'remove_sensor'), []),
    (T('Configure Server'), False, URL('home_server', 'configure_server'), [])
    ]



DEVELOPMENT_MENU = True

#########################################################################
## provide shortcuts for development. remove in production
#########################################################################

def _():
    # shortcuts
    app = request.application
    ctr = request.controller
    # useful links to internal and external resources

if DEVELOPMENT_MENU: _()

if "auth" in locals(): auth.wikimenu() 
